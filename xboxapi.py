import requests
import json
import os.path
import os
import urllib
import xbox
from pymongo import MongoClient
from pprint import pprint
from datetime import datetime, timedelta

global config 

class Gamer(object):
    def __init__(self, name):
        self.name = name
        self.xuid = ""
        self.game = ""
        self.state = ""
        self.gamertag = ""
        self.clips = []
        self.getId(name)

    def getId(self, name):
        url = "https://profile.xboxlive.com/users/gt(%s)/profile/settings?settings=Gamertag" % name
        headers = {'x-xbl-contract-version': 2}
        data = xbox.client._get(url, headers=headers).json()
        self.xuid = data['profileUsers'][0]['id']
        self.gamertag = data['profileUsers'][0]['settings'][0]['value']
        #return data['profileUsers'][0]['id']

    def getPresence(self):
        url = "https://userpresence.xboxlive.com/users/xuid(%s)" % self.xuid
        data = xbox.client._get(url).json()
        print data
        self.state = data['state']
        try:
            self.game = data['devices'][0]['titles'][1]['name']
        except:
            try:
                self.game = data['devices'][0]['titles'][0]['name']
            except:
                self.game = "None"

    def getClips(self):
        url = "https://gameclipsmetadata.xboxlive.com/users/xuid(%s)/clips" % self.xuid
        data = xbox.client._get(url).json()
        #print data["gameClips"][0]["datePublished"]
        for clip in data["gameClips"]:
            self.clips.append(clip["gameClipId"] + "," + clip["scid"])
        #print time

def discord(message):
    content = { 'content': message }
    requests.post(config['config']['discord_webhook'], content)
    print content

def getClipUrl(clip, xuid):
    data = clip.split(",")
    clipId = data[0]
    scid = data[1]
    url = "https://gameclipsmetadata.xboxlive.com/users/xuid(%s)/scids/%s/clips/%s" % (xuid, scid, clipId)
    data = xbox.client._get(url).json()
    return data["gameClip"]["gameClipUris"][0]["uri"]

def shortUrl(longurl):
    url = "https://www.googleapis.com/urlshortener/v1/url?key=%s" % config['config']['google_apikey']
    payload = {"longUrl": longurl}
    headers = {"Content-type": "application/json"}
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    return r.json()["id"]

with open('config.json') as config_file:
    config = json.load(config_file)

xbox.client.authenticate(login=config['config']['ms_user'], password=config['config']['ms_pass'])

gamers = [ "youwantchris", "Exhaust%20X", "n0ki", "Valiquin8", "Sinjed" ]
#gamers = ["youwantchris"]
filter_games = [ "Home", "Xbox App", "Plex for Xbox One", "Store", "Youtube" , "Pandora" ]

client = MongoClient('mongodb://192.168.2.20:27017')
db = client.home
message = "None"

for gamer in gamers:
    data = Gamer(gamer)
    data.getPresence()
    data.getClips()

    if any(value in data.game for value in filter_games):
        data.state = "Offline"
        data.game = "None"

    player = {
        'xuid': data.xuid,
        'gamertag': data.gamertag,
        'game': data.game,
        'state': data.state,
        'clips': data.clips
    }
    result = db.xbox.find({ "gamertag": data.gamertag})

    if result.count() > 0:
        previous = list(result)
        if previous[0]['state'] != data.state and data.state == "Offline":
            discord("%s is now offline." % data.gamertag)
        elif previous[0]['state'] != data.state and data.game != "None":
            discord("%s is now online and playing %s" % (data.gamertag, data.game))
        elif previous[0]['game'] != data.game and data.game != "None":
            discord("%s is now playing %s" % (data.gamertag, data.game))
        elif previous[0]['game'] != data.game and data.game == "None":
            discord("%s is done playing %s" % (data.gamertag, previous[0]['game']))
        try:
            clips = set(data.clips) - set(previous[0]["clips"])
        except:
            raise Exception("Error in clips")
        if len(clips) == 1 and data.state == "Online":
            for clip in clips:
 #               url = getClipUrl(clip, data.xuid)
                clip_arr = clip.split(",")
                url = "https://chris.home.gaikwad.org/xbox/%s/%s/%s" % (data.xuid, clip_arr[0], clip_arr[1])
#                discord("%s recorded a new clip - %s" % (data.gamertag, shortUrl(url)))
                discord("%s recorded a new clip - %s" % (data.gamertag, url))
        if len(data.clips) > 0:
            update = {
                '$set' : {
                    'game': data.game,
                    'state': data.state,
                    'clips': data.clips
                }
            }
        else:
            update = {
                '$set' : {
                    'game': data.game,
                    'state': data.state
                }
            }
        db.xbox.update({"gamertag": data.gamertag}, update, upsert=False)
    else:
        db.xbox.insert_one(player)

    # if message != "None":
    #     content = { 'content': message }
    #     #requests.post(config['config']['discord_webhook'], content)
    #     message = "None"
