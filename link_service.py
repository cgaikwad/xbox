from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import xbox
import json

class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        url = getClipUrl(self.path)
        #self.wfile.write("<p>You accessed path: %s</p>" % url)
        self.send_response(301)
        self.send_header('Location',url)
        self.end_headers()

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        # Doesn't do anything with posted data
        self._set_headers()
        self.wfile.write("<html><body><h1>POST!</h1></body></html>")

def getClipUrl(path):
    data = path.split("/")
    print data
    xuid = data[2]
    clipid = data[3]
    scid = data[4]
    url = "https://gameclipsmetadata.xboxlive.com/users/xuid(%s)/scids/%s/clips/%s" % (xuid, scid, clipid)
    print url
    data = xbox.client._get(url).json()
    return data["gameClip"]["gameClipUris"][0]["uri"]

def run(server_class=HTTPServer, handler_class=S, port=8080):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv
    with open('config.json') as config_file:
        config = json.load(config_file)

    xbox.client.authenticate(login=config['config']['ms_user'], password=config['config']['ms_pass'])

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()

