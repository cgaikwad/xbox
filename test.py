

import requests
import json
import os.path
import os
import urllib

global baseUrl
global headers

game = "Fortnite Battle Royale"

def getId(name):
    url = baseUrl + "/xuid/" + name
    r = requests.get(url, headers=headers)
    return r.json()

def getPresence(id):
    url = baseUrl + "/" + id + "/presence"
    r = requests.get(url, headers=headers)
    presence = r.json() 
    lockFile = "/home/chris/dev/" + id + ".lock"
    try:
        if presence['state'] == "Online":
            if os.path.exists(lockFile):
                print "Lock file exists for " + id
            else:
                for title in presence['devices'][0]['titles']:
                    if title['name'] != 'Home' and title['name'] != 'Xbox App' and title['state'] == "Active":
                        open(lockFile, 'a').close()
                        return title['name']
        else:
            if os.path.exists(lockFile):
                os.remove(lockFile)
                content = { 'content': urllib.unquote(gamer).decode('utf8') + ' is done.' }
                requests.post(webhook, content)
    except:
        if os.path.exists(lockFile):
            os.remove(lockFile)
            content = { 'content': urllib.unquote(gamer).decode('utf8') + ' is done.' }
            requests.post(webhook, content)

    return "None"

with open('config.json') as config_file:
    data = json.load(config_file)

baseUrl = "https://demo0203282.mockable.io/"
webhook = data['config']['discord_webhook']
apikey = data['config']['apikey']
headers = { 'X-AUTH': apikey }
                
gamers = [ "youwantchris", "Exhaust%20X", "n0ki", "Valiquin8" ]

for gamer in gamers:
    #game = getPresence(str(getId(gamer)))
    print getId(gamer)
    if game != "None":
        content = { 'content': urllib.unquote(gamer).decode('utf8') + ' is online playing ' + game }
        #requests.post(webhook, content)
        print content
